# Display network configuration
sudo nmcli

# Edit network configuration
sudo nmtui

# Install Fedora Red Hat Linux CoreOS with an ignition file
sudo coreos-installer install /dev/sda --copy-network --ignition-url=https://raw.githubusercontent.com/Younest9/fedoraos-config/main/config.yaml
