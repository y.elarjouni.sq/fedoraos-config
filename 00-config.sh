# Install podman
apt install podman

# Create an alias to use butane
alias butane='podman run --rm --interactive       \
              --security-opt label=disable        \
              --volume ${PWD}:/pwd --workdir /pwd \
              quay.io/coreos/butane:release'  # Use butane's official image

# Pull the image
podman pull quay.io/coreos/butane:release 

# Generate the ignition file from the yaml
butane --pretty --strict https://raw.githubusercontent.com/Younest9/fedoraos-config/main/config.yaml | tee config.ign 





## Ressources

# https://github.com/coreos/butane/blob/main/docs/getting-started.md
# https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/
# https://docs.fedoraproject.org/en-US/fedora-coreos/authentication/
# https://docs.fedoraproject.org/en-US/fedora-coreos/hostname/
# https://docs.fedoraproject.org/en-US/fedora-coreos/sysconfig-setting-keymap/
# https://docs.fedoraproject.org/en-US/fedora-coreos/time-zone/
